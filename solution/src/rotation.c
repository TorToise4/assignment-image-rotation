#include "bmp_cast.h"
#include "image.h"
#include "rotation.h"
#include "stdlib.h"

struct image rotate( struct image const source ){
    struct image rotated_image;
    rotated_image.width = source.height;
    rotated_image.height = source.width;
    rotated_image.data = malloc(source.width * source.height * sizeof(struct pixel));
    for (size_t i = 0; i < source.height; ++i){
        for (size_t j = 0; j < source.width; ++j){
            rotated_image.data[(rotated_image.width-1-i)+(rotated_image.width*j)] = source.data[source.width*i+j];
        }
    }
    free(source.data);
    return rotated_image;
}
