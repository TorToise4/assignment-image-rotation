#include "inttypes.h"

#include "open-close.h"

static const char* const flags[]={
    [RB] = "rb",
    [WB] = "wb",
};

enum open_status open_read(const char* file_in, FILE** in, enum open_flags flag){
    if(flag != RB){
        return OPEN_ERROR;
    } else{
        *in = fopen(file_in, flags[flag]);
        if (in == NULL){
            return OPEN_ERROR;
        }
        return OPEN_OK;
    }
}

enum open_status open_write(const char* file_out, FILE** in, enum open_flags flag){
    if(flag != WB){
        return OPEN_ERROR;
    } else{
        *in = fopen(file_out, flags[flag]);
        if (in == NULL){
            return OPEN_ERROR;
        }
        return OPEN_OK;
    }
}
