
#include "bmp_cast.h"
#include "inttypes.h"
#include "open-close.h"
#include "rotation.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"



int main(int argc, char **argv){
    if (argc!=3){
        return 1;
    }
    const char* file_in = argv[1];
    const char* file_out = argv[2];
    FILE *in;
    open_read(file_in, &in, RB);
    struct image image;
    from_bmp(in, &image);
    fclose(in);
    struct image rotated_image = rotate(image);
    FILE *out;
    open_write(file_out, &out, WB);
    to_bmp(out, &rotated_image);
    free(rotated_image.data);
    fclose(out);
    return 0;
}
