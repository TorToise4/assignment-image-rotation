#include "inttypes.h"

#include "bmp_cast.h"
#include "image.h"
#include "stdlib.h"

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)



static uint8_t padding(struct image const* image){
    if (image->width % 4 != 0){
        return image->width % 4;
    }
    return 0;
}

static struct bmp_header make_header(struct image const* image){
    struct bmp_header header;
    header.bfType = 19778;
    header.bfileSize = image->height * image->width * sizeof(struct pixel) + sizeof(header);
    header.bfReserved = 0;
    header.bOffBits = 54;
    header.biSize = 40;
    header.biHeight = image->height;
    header.biWidth = image->width;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = image->height * image->width * sizeof (struct pixel);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

static struct image make_image(struct bmp_header header){
    struct image image;
    image.height = header.biHeight;
    image.width = header.biWidth;
    image.data = malloc(image.width * image.height * sizeof(struct pixel));
    return image;
}

enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header header;
    const size_t f_header = fread(&header, sizeof (header), 1, in);
    if (f_header != 1){
        return READ_INVALID_HEADER;
    }
    struct image image_1 = make_image(header);
    const int header_seek = fseek(in, header.bOffBits, SEEK_SET);
    if (header_seek != 0){
        free(image_1.data);
        return READ_INVALID_SEEK;
    }
    size_t f_pixel;
    int padding_seek;
    for (size_t i = 0; i < image_1.height; ++i) {
        for (size_t j = 0; j < image_1.width; ++j){
            f_pixel = fread(&image_1.data[i * image_1.width + j], sizeof(struct pixel), 1, in);
            if (f_pixel != 1){
                return READ_INVALID_PIXELS;
            }
        }
        padding_seek = fseek(in, padding(&image_1), SEEK_CUR);
        if (padding_seek != 0){
            return READ_INVALID_SEEK;
        }
    }
    *img = image_1;
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    struct bmp_header header = make_header(img);
    const int zero_seek = fseek(out, 0, SEEK_SET);
    if (zero_seek != 0){
        return WRITE_ERROR;
    }
    const size_t header_write = fwrite(&header, sizeof (struct bmp_header),1,out);
    if (header_write != 1){
        return WRITE_ERROR;
    }
    const int header_seek = fseek(out, header.bOffBits, SEEK_SET);
    if (header_seek != 0){
        return WRITE_ERROR;
    }
    size_t pixel_write;
    const uint8_t four_element_massive[4] = {0, 0, 0, 0};
    for (size_t i = 0; i < img->height; ++i) {
        for (size_t  j = 0; j < img->width; ++j){
            pixel_write = fwrite(&img->data[i * img->width + j], sizeof (struct pixel), 1, out);
            if (pixel_write != 1){
                return WRITE_ERROR;
            }
        }
        fwrite(&four_element_massive,sizeof(uint8_t), padding(img), out);

    }
    return WRITE_OK;
}
