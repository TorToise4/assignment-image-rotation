

#ifndef ASSIGNMENT_IMAGE_ROTATION_2_OPEN_CLOSE_H
#define ASSIGNMENT_IMAGE_ROTATION_2_OPEN_CLOSE_H

#endif //ASSIGNMENT_IMAGE_ROTATION_2_OPEN_CLOSE_H
#pragma once
#include "stdint.h"
#include "stdio.h"


enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR
};

enum open_flags {
    RB = 0,
    WB
};

enum open_status open_read(const char* file_in, FILE** in, enum open_flags flag);

enum open_status open_write(const char* file_out, FILE** in, enum open_flags flag);
